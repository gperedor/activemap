(ns activemap.benchmarks
  (:use activemap.core
        activemap.meta.types)
  (:require [activemap.fixtures :as fix]
            [clojure.java.jdbc :as sql]
            [activemap.test-util :as util]
            [clojure.java.io :as io]))

(defmacro void
  [& corpus]
  `(do ~@corpus
       nil))

(defmacro time-benchmark
  [expr]
  `(let [start# (java.util.Date.)
         ret# ~expr
         end# (java.util.Date.)
         t# (/ (- (double (.getTime end#)) (.getTime start#)) 1000.0)
         ]
     (println (format "Elapsed time: %1$.2f seconds" t#))
     t#))

;;; Select n Travel records

;;; Select all destination cities from n Travel records

;;; Insert m City records

;;; Delete l Travel records

;;; Update k Travel records

;;; Update k Travel records, including cities

;;; Transaction support

(defn select-travel
  []
  (doall @(select fix/travel)))

(defn select-destinations
  []
  (doall (map (comp force :destination)
              @(select fix/travel {:date [:> (util/make-date 2010 1 1)]}))))

(defn insert-cities
  []
  (sql/transaction
   (let [[s]           @(limit-records 1 (select fix/state))
         template-city {:foundation nil
                        :state s}
         n             @(count-records (select fix/city))]
     (doseq [i (range n (+ n 1000))]
       (insert! (hash-map->record-map fix/city
                                      (assoc template-city
                                        :name
                                        (str "City " i))))))))

(def travels
  (sql/with-connection util/postgres-benchmarks
    @(limit-records 1000
                    (select fix/travel))))

;; (println (reference-hash-map @(:origin (:refs fix/travel)) 
;;                              @(:origin (first @(select fix/travel)))))

(defn delete-travels
  []
  (sql/transaction
   (doseq [t travels] (delete! t))))

(defn update-travels
  []
  (sql/transaction
   (doseq [t travels]
     (update! (assoc t :date (java.util.Date.))))))

(defn update-destinations
  []
  (sql/transaction
   (doseq [t travels]
     (let [d @(:destination t)]
       (update! (assoc d :foundation (java.util.Date.)))
       (update! (assoc t :date (java.util.Date.)))))))

(defn run-pure
  [f]
  (sql/with-connection util/postgres-benchmarks
    (time-benchmark (f))))

(defn run-impure
  [f table]
  (sql/with-connection util/postgres-benchmarks
    (time-benchmark (f)))
  (sql/with-connection util/postgres-benchmarks
    (sql/do-commands (str "truncate " table))
    (sql/do-commands (str "insert into " table
                        " select * from " table "_bkp;"))))

;; (defn run-benchmarks
;;   []
;;   (let [pure-bms [select-travel select-destinations]
;;         impure-bms [[insert-cities "cities.sql"]
;;                     [update-travels "travels.sql"]
;;                     [delete-travels "travels.sql"]]]
;;     (doseq [benchmark pure-bms]
;;       (run-pure benchmark))
;;     (doseq [[benchmark file] impure-bms]
;;       (run-impure benchmark (io/resource file)))))

(def benchmarks-map
  {"select-travel" #(run-pure select-travel)
   "select-destinations" #(run-pure select-destinations)
   "update-travels" #(run-impure update-travels "travel")
   "insert-cities" #(run-impure insert-cities "city")
   "delete-travels" #(run-impure delete-travels "travel")})

(defn run-benchmark
  [benchmark & _]
  (let [times (->> (for [i (range 100)]
                     ((get benchmarks-map benchmark)))
                   (sort))
        avg (/ (reduce + 0 times) 100.0)]
    (println "Average\tMedian\tStd. dev")
    (print avg "\t")
    (print (nth times 49) "\t")
    (println (Math/sqrt (/ (reduce + 0
                                   (map #(Math/pow (- % avg) 2)
                                        times))
                           100)))))


(try (run-benchmark "update-travels")
     (catch Exception e
       (.printStackTrace e)))
