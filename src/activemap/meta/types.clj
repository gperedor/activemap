(ns activemap.meta.types
  (:use potemkin)
  (:require [activemap.util :as util]
            [clojure.string :as string]
            clojure.set))

(declare rmap-coerce)

(defrecord Entity-type
  [table-name                 ;; Name of table (keyword)
   atts                       ;; Attributes
   refs                       ;; References
   colls                      ;; Collections
   colls-through              ;; Many-to-many collections
   col-types                  ;; Necessary for querying
   ])

(defrecord Reference
    [key-name
     to-entity
     pk
     not-null
     fks-pks ;; {:fk1 :pk1 :fk2 :pk2 ...}
     ])

(defrecord Collection
  [key-name
   from-entity
   foreign-name
   ref])

(defrecord CollectionThrough
    [key-name
     outgoing-entity
     mapping-table
     incoming-fks
     outgoing-fks])

(defn- otni
  [to from]
  (into from to))

(defmethod print-method Collection [o ^java.io.Writer w]
  (let [fake-ref    "#<Delay (print suppressed by Activemap)>"
        fake-entity (str "#" (.getName (class (:from-entity o)))
                         "{(print suppressed by Activemap)} ")]
    (.write w (str "#" (.getName (class o))))
    (-> (assoc o :ref fake-ref :from-entity fake-entity)
        (otni {})
        (print-method w))))

(defmethod print-method CollectionThrough [o ^java.io.Writer w]
  (let [fake-entity (str "#" (.getName (class (:outgoing-entity o)))
                         "{(print suppressed by Activemap)} ")]
    (.write w (str "#" (.getName (class o))))
    (-> (assoc o :outgoing-entity fake-entity)
        (otni {})
        (print-method w))))

(defrecord Attribute
  [key-name
   col-name
   data-type
   auto-inc
   pk
   not-null])

(derive Reference ::ref-type)

(def input-formats
  {:rut "#.###.###-#"
   :number "#,###,###"
   :decimal "#.##0,##"})

(defn is-type? [value datatype] (isa? (type value) datatype))

(defn table-name
  [instance]
  (cond
    (is-type? instance Entity-type)
    (:table-name instance)
    (is-type? instance Reference)
    (:table-name (:to-entity instance))
    :else
    nil))

(defn pk-attributes
  "Attribute objects which are part of entity-type's primary key"
  [entity-type]
  (into {} (filter (comp :pk val) (:atts entity-type))))

(defn pk-refs
  "Reference objects which are part of entity-type's primary key"
  [entity-type]
  (into {} (filter (comp :pk force val) (:refs entity-type))))

(defn pk-columns
  "Database column names which constitute entity-type's primary key"
  [entity-type]
  (concat (map :col-name (vals (pk-attributes entity-type)))
          (mapcat (comp keys :fks-pks force) (vals (pk-refs entity-type)))))

(defn key-name->col-name
  [entity-type]
  (util/map-hash-map (fn [[key-name attribute]]
                          [key-name (:col-name attribute)])
                        (:atts entity-type)))

(defn col-name->key-name
  [entity-type]
  (util/map-hash-map (fn [[key-name attribute]]
                       [(:col-name attribute) key-name])
                     (:atts entity-type)))

(defn col-names
  "Names of all concrete column names associated with an Entity-type's given table"
  [entity-type]
  (concat (map :col-name
               (vals (:atts entity-type)))
          (mapcat (comp keys :fks-pks force)
                  (vals (:refs entity-type)))))

(defn- delay-if-not
  [v]
  (if (delay? v)
    v
    (delay v)))

(defn not-nullable?
  [p]
  (or (:pk p) (:not-null p)))

(def-map-type RMap
  [entity-type
   dirty?
   original-pk
   atts
   refs
   colls]
  (get [this k default-value]
       (cond (contains? atts k)  (get atts k)
             (contains? refs k)  (get refs k)
             (contains? colls k) (get colls k)
             :else              default-value))
  (assoc [_ k v]
    (cond (some #{k} (keys (:refs entity-type)))
          (RMap. entity-type
                 true
                 original-pk
                 (dissoc atts k)
                 (assoc refs k (delay-if-not v))
                 (dissoc atts k))

          (some #{k} (keys (:colls entity-type)))
          (RMap. entity-type
                 true
                 original-pk
                 (dissoc atts k)
                 (dissoc refs k)
                 (assoc atts k (delay-if-not v)))

          :else
          (RMap. entity-type
                 true
                 original-pk
                 (assoc atts k v)
                 (dissoc refs k)
                 (dissoc colls k))))

  (dissoc [_ k]
          (RMap. entity-type true original-pk (dissoc atts k) (dissoc refs k) (dissoc colls k)))
  (keys [_]
        (concat (keys atts) (keys refs) (keys colls)))
  
  java.lang.Object
  (hashCode [this]
            (reduce
             (fn [acc [k v]]
               (unchecked-add acc (bit-xor (hash k) (hash v))))
             0
             (seq atts))))

(defn fresh-rmap
  "Factory function. For RMaps queried from the database"
  [entity-type original-pk atts refs colls]
  (->RMap entity-type false original-pk atts refs colls))

(defn dirty-rmap
  "Factory function. For RMaps made anew or from other RMaps"
  [entity-type atts refs colls]
  (->RMap entity-type true {} atts refs colls))

(defn attributes
  "Slices only attributes from a record-map"
  [record-map]
  (.atts record-map))

(defn refs
  "Slices only references from a record-map"
  [record-map]
  (.refs record-map))

(defn colls
  "Slices only collections from a record-map"
  [record-map]
  (.colls record-map))

(defmulti type-coerce (fn [pair type] type))

(defmethod type-coerce :integer
  [[key value] type]
  (try [{key (Integer/parseInt value)} {}]
       (catch Exception e
         (.printStackTrace e)
         [{} {key :integer}])))

(defmethod type-coerce :default
  [[key value] type]
  [{key value} {}])

(defn- coerce-attributes
  [entity-type m]
  (->> (select-keys (:atts entity-type) (keys m))
       (reduce (fn [[coerced errors] [k att]]
                 (let [[c e] (type-coerce [k (get m k)] (:data-type att))]
                      [(merge coerced c) (merge errors e)]))
               [{} {}])))

(defn- coerce-references
  [entity-type m]
  (->> (select-keys (:refs entity-type) (keys m))
       (reduce (fn [[coerced errors] [k ref]]
                 (let [ref-entity (:to-entity @(get-in entity-type [:refs k]))
                       [c e] (rmap-coerce ref-entity (get m k))]
                   [(if-not (empty? c)
                      (assoc coerced k c)
                      coerced) 
                    (if-not (empty? e)
                      (assoc errors k e)
                      errors)]))
               [{} {}])))

(defn rmap-coerce
  "Attempts to coerce a hash-map's values according to the entity's specified types"
  [entity-type m]
  (let [[coerced-atts atts-errors] (coerce-attributes entity-type m)
        [coerced-refs refs-errors] (coerce-references entity-type m)]
    [(merge coerced-atts coerced-refs) (merge atts-errors refs-errors)]))
