(ns activemap.meta.ddl
  (:use activemap.meta.types)
  (:require [activemap.util :as util]
            [lobos.schema :as schema]
            [lobos.core :as lobos]
            [clojure.string :as string]
            clojure.set))

(declare table-columns foreign-attributes)

(defn attribute*
  [att-name properties]
  (let [default-name (keyword (string/replace (name att-name) #"-" "_"))
        defaults {:key-name att-name  :col-name default-name :data-type :text
                  :auto-inc false :pk false      :not-null false}]
    (map->Attribute (merge defaults properties))))

(defn attribute
  "Constructs an attribute object. Properties, with associated defaults, include:

{:col-name name  :data-type :text
 :auto-inc false :pk false      :not-null false}
"
  ([entity name]
     (attribute entity name {}))
  ([entity name properties]
     (assoc-in entity [:atts name]
               (attribute* name properties))))

(defn reference*
  ([name to-entity]
     (reference* name to-entity {}))
  ([name to-entity properties]
     (let [default-fks (util/map-hash-map 
                        (fn [pk]
                          [(util/keyword-join "_" name pk) pk])
                        (keys (merge (pk-attributes to-entity)
                                     (pk-refs to-entity))))
           defaults    {:key-name name :to-entity to-entity
                        :pk false      :not-null false
                        :fks-pks default-fks}]
       (map->Reference (merge defaults
                              properties)))))

(defn with-delayed-entity
  [f & entities]
  (delay
   (apply f (map deref entities))))

(defmacro reference
  "Constructs a delayed reference object.
Properties, with associated default values, include:

:key-name name :to-entity to-entity
:pk false      :not-null false
:fks-pks default-fks

With default fks being the pks declared for the referred entity, with  (reference-name)_ prepended"
  ([entity reference-name to-entity]
     `(reference ~entity ~reference-name ~to-entity {}))
  ([entity reference-name to-entity properties]
     (let [placeholder `(var ~to-entity)]
       `(assoc-in ~entity [:refs ~reference-name]
                  (with-delayed-entity #(reference* ~reference-name % ~properties)
                    ~placeholder)))))


(defn collection*
  [collection-name from-entity foreign-name]
  (->Collection collection-name
                from-entity
                foreign-name
                (get-in from-entity [:refs foreign-name])))

(defmacro collection
  [entity collection-name from-entity foreign-name]
     (let [placeholder `(var ~from-entity)]
       `(assoc-in ~entity [:colls ~collection-name]
                  (with-delayed-entity  #(collection* ~collection-name % ~foreign-name)
                    ~placeholder))))

(defn col-types
  [entity-type]
  (let [atts (into {} (map (fn [[_ {:keys [col-name data-type]}]]
                             [col-name data-type])
                           (:atts entity-type)))
        refs (apply merge (map (fn [[_ r]]
                                 (let [pks-fks (clojure.set/map-invert (:fks-pks @r))]
                                   (util/project @(:col-types (:to-entity @r))
                                                 pks-fks)))
                               (:refs entity-type)))]
    (merge atts refs)))


(defn- mapping-columns
  [entity-type]
  (util/map-hash-map
   (fn [pk]
     [(util/keyword-join "_" 
                         (:table-name entity-type)
                         (name pk))
      pk])
   (pk-columns entity-type)))

(defn collection-through*
  [entity-type collection-name outgoing-entity properties]
  (let [defaults {:mapping-table (util/keyword-join "_"
                                                    (:table-name entity-type)
                                                    (:table-name outgoing-entity))
                  :incoming-fks (mapping-columns entity-type)
                  :outgoing-fks (mapping-columns outgoing-entity)
                  :outgoing-entity outgoing-entity
                  :key-name collection-name}]
    (map->CollectionThrough (merge defaults properties))))

(defmacro collection-through
  ([entity collection-name outgoing-entity]
     `(collection-through ~entity ~collection-name ~outgoing-entity {}))
  ([entity collection-name outgoing-entity properties]
     (let [out-placeholder `(var ~outgoing-entity)]
       `(let [computed-entity# ~entity]
          (println computed-entity#)
          (assoc-in computed-entity# [:colls-through ~collection-name]
                    (with-delayed-entity (fn [out#]
                                           (collection-through* computed-entity#
                                                                ~collection-name
                                                                out#
                                                                ~properties))
                      ~out-placeholder))))))

(defmacro defentity
"Entity definition, returns an Entity-type object. Example:

(defentity state
  {:table-name :state}
  (attribute :name {:pk true :not-null true}))

(defentity city
  {:table-name :city}
  (attribute :name {:pk true :not-null true})
  (attribute :foundation {:data-type :date})
  (reference :state state {:pk true :not-null true}))

Defaults:
:table-name The entity's name concatenated with an 's'
"
[entity-name properties & body]
(let [defaults {:table-name (keyword (str (name entity-name) "s"))}
      options `(merge ~defaults ~properties)]
  `(def ~entity-name (-> (map->Entity-type (assoc ~options
                                             :col-types
                                             (delay (col-types ~entity-name))))
                  ~@body))))

(defn attribute->column
  "Constructs a lobos column object from a hash-map"
  [attribute]
  (let [ltype  (:data-type attribute)
        options (filter (get attribute) #{:auto-inc :not-null})]
    (apply schema/column
           (:col-name attribute)
           ltype
           options)))

(defn table-columns
  "Returns a list of map-types containing the necessary options for lobos' column function"
  [entity-type]
  (let [att-cols (vals (:atts entity-type))
        ref-cols (mapcat (fn [[_ reference]]
                           (foreign-attributes reference))
                         (:refs entity-type))]
    (concat att-cols ref-cols)))

(defn foreign-attributes
  "Creates a new hash containing the necessary keys for attribute->column"
  [reference]
  ;; Returning straight-out Attribute objects and modifying them would yield inconsistent objects, and this is not desirable
  (let [{:keys [fks-pks to-entity]} @reference
        mapping (clojure.set/map-invert fks-pks)]
    (map (fn [k]
           (-> (select-keys k [:col-name :pk :not-null :data-type]) 
               (update-in [:col-name] mapping)))
         (filter (fn [{col-name :col-name}]
                   (contains? mapping col-name))
                 (table-columns to-entity)))))

(defn create-table!
  "Creates a table from entity-type"
  [entity-type]
  (let [columns (table-columns entity-type)
        pk-columns (filter :pk columns)
        lobos-columns (map attribute->column columns)]
    (lobos/create (-> (schema/table* (:table-name entity-type)
                                     lobos-columns)
                      (schema/primary-key (map :col-name pk-columns))))))
