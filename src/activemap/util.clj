(ns activemap.util
  (:require [clojure.set :as sets]))

(defn merge-in
  "The right-hand map into the left-hand map following the mediating keys"
  [left [k & ks :as keys] right]
  (if (empty? keys)
    (merge left right)
    (assoc left k (merge-in (get left k)
                               ks
                               right))))
(defn filter-keys
  [dict predicate]
  (select-keys dict (filter predicate (keys dict))))

(defmacro dbg[x] `(let [x# ~x] (println "dbg:" '~x "=" x#) (println "\tType = " (type x#)) x#))

(defn to-class
  [s & args]
  (clojure.lang.Reflector/invokeConstructor
   (resolve (symbol s))
   (to-array args)))

(defn concat-symbol
  [left right]
  (symbol (str (subs (str left) 1)
               (subs (str right) 1))))

(defn keyword-join
  [inter & symbols]
  (keyword (clojure.string/join (name inter) (map name symbols))))

(defn map-hash-map
  "Constructs a hash-map from the result of applying
proc, a function of the form a -> [k, v] to the elements of col"
  ([proc collection]
     (map-hash-map proc {} collection))
  ([proc base collection]
     (reduce (fn [hm value]
               (let [[k v] (proc value)]
                 (assoc hm k v)))
             base
             collection)))

(defn map-merge
  [f coll]
  (reduce (fn [m point]
            (merge m (f point)))
          {}
          coll))

(defn categorize
  [collection sorter]
  (reduce (fn [aggregate element]
            (let [category (sorter element)]
              (assoc aggregate
                category
                (conj (get aggregate category []) element))))
          {}
          collection))

(defn equivalent-subset
  "Given a mapping of keys, construct a map from associative with the keys translated"
  [associative key-mapping]
  (reduce (fn [m [k v]]
            (if (contains? key-mapping k)
              (assoc m (get key-mapping k) v)
              m))
          {}
          associative))

(defn project
  "Given a mapping of keys, construct a map from associative with the keys translated, and nil in the case they are not contained in it"
  [associative key-mapping]
 (reduce (fn [m [k1 k2]]
            (assoc m k2 (get associative k1)))
          {}
          key-mapping))

(defn translate-keys
  "Given a mapping of keys, change those keys in associative to their equivalent in key-mapping"
  [associative key-mapping]
  (merge (apply dissoc associative key-mapping)
         (equivalent-subset associative key-mapping)))

(defn attribute-values
  "Selects keys from a map corresponding to attributes"
  [entity-type m]
  (select-keys m (keys (:atts entity-type))))

(defn reference-values
  "Selects keys from a map corresponding to references"
  [entity-type m]
  (select-keys m (keys (:refs entity-type))))
