(ns activemap.core
  (:use
   [clojureql.core :only [where table join project]]
   clojureql.predicates
   activemap.meta.types
   [clojure.core.match :only [match]])
  (:require clojure.set
            clojure.string
            [clojure.zip :as zip]
            [clojureql.core :as cljql]
            [activemap.util :as util]
            [clojure.java.jdbc :as sql]))

;;; These are part of mutally recursive functions down the line
(declare hash-map->record-map lazy-ref full-select row->record-map lazy-coll)

(prefer-method print-method clojure.lang.IRecord clojure.lang.IDeref)
(prefer-method print-method clojure.lang.IPersistentMap clojure.lang.IDeref)

(defrecord TQuery
    [entity-type criteria count? limit]
;;; This'll be !!FUN!!
  clojure.lang.IDeref
  (deref [this]
    (let [[aliases body] (full-select entity-type criteria)]
      (let [query (if-not (nil? limit)
                    (cljql/limit body limit)
                    body)]
        (if count?
          (-> query
              (cljql/aggregate [[:count/* :as :heh]])
              deref
              first
              :heh)
          (map (partial row->record-map entity-type aliases)
               @query))))))

(def logical-operators #{:or :and :not})

(def qualify (partial util/keyword-join "."))

(defn- apply-if-many
  "Applies function if coll has more than one element, else, returns said element"
  [f [x & xs :as coll]]
  (if (empty? xs)
    x
    (apply f coll)))

(defn project-aliases
  [[aliases query] fields]
  (let [reference-alias (:entity-alias (meta aliases))]
    (project
     query
     (into [] (map (fn [[alias column-name]]
                     [(qualify reference-alias column-name) :as alias])
                   (select-keys aliases fields))))))

;;; bare gensym avoids problems with max col name length in the RDBMs
;;; over encoding information on the column alias (e.g for later
;;; synthesis of the final record)
(defn- generate-alias
  []
  (keyword (gensym "g__")))

(let [n *ns*]
  (defn- keyword-to-op [op] (if (not= op :like)
                              (get (ns-map n) (symbol (str (name op) "*")))
                              like)))

(defn- coerce-type
  [data-type object]
  (case data-type
    :date (java.sql.Date. (.getTime object))
    :time (java.sql.Time. (.getTime object))
    :timestamp (java.sql.Timestamp. (.getTime object))
    object))

(defn attribute-predicate
  "Constructs a ClojureQL selection proposition for the given attribute"
  [entity-alias [attribute {:keys [conditions type]}]]
  (letfn [(expression-tree [expression]
            (let [root (zip/vector-zip expression)]
              (if (zip/branch? root)
                [(apply (keyword-to-op (-> root zip/down zip/node))
                        (mapcat expression-tree (-> root zip/down zip/rights)))]
                [(qualify entity-alias attribute) (and expression
                                                       (coerce-type type
                                                                    expression))])))]
    (first (expression-tree conditions))))

(defn attribute-conditions
  "Constructs a list of ClojureQL predicates from a hashmap containing TQuery conditions"
  [entity-type entity-alias attributes]
  (let [translated-attributes (util/map-hash-map
                               (fn [[a v]]
                                 [(get-in entity-type [:atts a :col-name])
                                  {:conditions v
                                   :type (get-in entity-type [:atts a :data-type])}])
                               attributes)]
    (map (partial attribute-predicate entity-alias)
         translated-attributes)))

(defn- arbitrary-cols-conditions
  [entity-type col-vals]
  (util/map-hash-map
   (fn [[col val]]
     [col {:conditions [:= val]
           :type (get @(:col-types entity-type) col)}])
   col-vals))

(defn basic-join-conditions
  "A list of ClojureQL predicates corresponding to equality predicates matching one entity's FKs to another's PKs"
  [entity-alias aliased-cols reference]
  (let [fks-pks (:fks-pks reference)
        reference-alias (:entity-alias (meta aliased-cols))]
    (map (fn [[fk pk]]
           (=* (qualify entity-alias fk)
               (qualify reference-alias pk)))
         (:fks-pks @reference))))

(defn- make-aliases
  "A function from [:col-names] -> {:alias :col-name}"
  [col-names]
  (into {} (map (fn [c] [(generate-alias) c])
                col-names)))

(defn join-statement
  "Query -> Keyword -> Reference -> Constraints -> [Query, {Aliases}]
Queries attributes of the referred entity. An alias for the source table is needed. Constraints in Temawork's usual format are optional"
  ([query source-alias reference]
     (join-statement query source-alias reference {}))
  ([query source-alias reference constraints]
     (let [to-entity        (:to-entity @reference)
           reference-alias  (generate-alias)
           aliased-cols     (-> (make-aliases (col-names to-entity))
                                (with-meta {:entity-alias reference-alias}))
           all-conditions     (concat
                               (basic-join-conditions source-alias
                                                      aliased-cols
                                                      reference)
                               (attribute-conditions  to-entity
                                                      reference-alias
                                                      constraints))  
           cljql-conditions (where (apply-if-many and*
                                                  all-conditions))
           table            (cljql/table {(:table-name to-entity) reference-alias})]

       [aliased-cols
        (join query
              (project-aliases [aliased-cols table] (keys aliased-cols))
              cljql-conditions)])))

(defn extract-ref-pks
  "Takes a slice of a row, corresponding to a reference's PK values"
  [reference aliases row]
  (-> (util/translate-keys row aliases)
      (util/project (:fks-pks @reference))))

(defn row->record-map
  "Transforms a given hashmap representing a row from a query into a
  record-representation, as per the given entity-type and aliases.

  For internal use."
  [entity-type aliases row]
  (cond (empty? row)
        nil
        :else
        (let [dealiased      (util/translate-keys row aliases)
              filter-columns (pk-columns entity-type)
              pk-values      (select-keys dealiased
                                          filter-columns)]
          (let [atts (util/map-hash-map (fn [[key-name attribute]]
                                          [key-name (get dealiased
                                                         (:col-name attribute))])
                                        (:atts entity-type))
                refs (util/map-hash-map
                      (fn [[key-name reference]]
                        [key-name (lazy-ref entity-type
                                            pk-values
                                            (extract-ref-pks reference
                                                             aliases
                                                             row)
                                            reference)])
                      (:refs entity-type))
                colls (util/map-hash-map
                       (fn [[key-name coll]]
                         [key-name (lazy-coll entity-type pk-values coll)])
                       (:colls entity-type))]
            (fresh-rmap entity-type pk-values atts refs colls)))))

(defn pk-hash-map
  "Extracts from a record object the declared primary keys, associated to the col-names"
  [record-map]
  (cond (empty? record-map)
        [{} {}]
        
        (not (.dirty? record-map))
        [{} (.original-pk record-map)]
        
        :else
        (let [entity-type  (.entity-type record-map)
              {:keys [table-name atts refs]} entity-type
              native-pks (-> (select-keys record-map (keys (pk-attributes entity-type)))
                             (util/equivalent-subset (key-name->col-name entity-type)))
              foreign-pks (->> (pk-refs entity-type)
                               (util/map-merge
                                (fn [[key-name ref]]
                                  (let [pks-fks (clojure.set/map-invert (:fks-pks @ref))
                                        fields-fks (util/project pks-fks
                                                                 (clojure.set/map-invert (:fks-pks @ref)))]
                                    (util/project (second (pk-hash-map (force (get record-map key-name))))
                                                  pks-fks)))))]
          [{} (merge native-pks foreign-pks)])))



(defn select-by-pk-row
  "Returns a full row from its PK. Row must contain only PK keys."
  [entity-type row]
  (let [entity-alias (generate-alias)
        preds (map (fn [a] (attribute-predicate entity-alias a) )
               (arbitrary-cols-conditions entity-type
                                          row))]
    [(with-meta {} {:entity-alias entity-alias})
     (cljql/select (cljql/table {(:table-name entity-type) entity-alias})
                   (where (apply-if-many and*
                                         preds)))]))

(defn select-by-pk
  "Returns the row corresponding to a record-map"
  [record-map]
  (let [entity-type (.entity-type record-map)]
    (select-by-pk-row entity-type (second (pk-hash-map record-map)))))

(defn lazy-ref
  "Returns a delayed record-map for a row's corresponding reference,
  queried from the database. All that is required from the row is to
  have its full PK"
  [entity-type row fk-values ref]
  (delay (let [to-entity (:to-entity @ref)]
           (if (not-empty fk-values)
             (row->record-map to-entity
                              {}
                              (first @(second (select-by-pk-row to-entity
                                                                fk-values))))
             (let [[aliases query] (select-by-pk-row entity-type row)
                   [ref-aliases join] (join-statement query
                                                      (:entity-alias (meta aliases))
                                                      ref)]
               (row->record-map to-entity ref-aliases (first @join)))))))

(defn lazy-coll
  "Returns a delayed seq of record-maps for the records of a foreign entity
   referring to this one"
  [entity-type row coll]
  (delay (let [ref        (:ref @coll)
               fk-values  (clojure.set/map-invert (:fks-pks @ref))
               
               from-table (get-in @coll [:from-entity :table-name])

               constraints (arbitrary-cols-conditions (:from-entity @coll)
                                                      (util/project row
                                                                    fk-values))
               cljql-conditions (map (fn [a] (attribute-predicate from-table
                                                                  a))
                                     constraints)
               query (cljql/select (cljql/table from-table)
                                   (apply-if-many and*
                                                  cljql-conditions))
               ;;; A small optimization; query the parent row just once
               [_ common-query] (select-by-pk-row entity-type row)
               common-ref     (delay (row->record-map entity-type
                                                      {}
                                                      (first @common-query)))]
           (for [row @query]
             (let [r (row->record-map (:from-entity @coll) {} row)
                   original-pk (.original-pk r)
                   atts        (.atts r)
                   refs        (.refs r)
                   colls       (.colls r)]
               ;;; The small optimization must go unnoticed...
               ;;; no dirtying the record
               (fresh-rmap (:from-entity @coll)
                           original-pk
                           atts
                           (assoc refs (:foreign-name @coll)
                                  common-ref)
                           colls))))))

(defn canonicalize
  [base [key-name ref]]
  (match [(get base key-name)]
         [nil]    [key-name nil]
         [record] [key-name (delay (hash-map->record-map (:to-entity @ref)
                                                         (force record)))]))
(defn hash-map->record-map
  "Canonicalizes a well-formed hash-map representing a record, adding
  the required metadata."
  [root-entity base]
  (let [atts (select-keys base (keys (:atts root-entity)))
        refs (util/map-hash-map
               (partial canonicalize base)
               (:refs root-entity))]
    (dirty-rmap root-entity atts refs {})))

(defn- formed-condition
  [v2]
  (cond (and (coll? v2) (keyword? (first v2)))
        (if (contains? logical-operators (first v2))
          (vec (cons (first v2) (map formed-condition (rest v2))))
          v2)
        (coll? v2)                              [:in v2]
        (nil?  v2)                              [:= nil]
        :else                                   [:= v2]))

(defn- merge-conditions
  [entity-type op conds1 conds2]
  (let [base-atts  (keys (:atts entity-type))
        base-refs  (keys (:refs entity-type))]
    (-> (reduce (fn [m k]
                  (->> (match [(contains? conds1 k) (get m k) (get conds2 k)]
                              [false _  v2] (formed-condition v2)
                              [true     v1 v2] [op v1 (formed-condition v2)])
                       (assoc m k)))
                (select-keys conds1 base-atts)
                (clojure.set/intersection (set (keys conds2))
                                          (set base-atts)))
        (merge (reduce (fn [m k]
                         (let [to-entity (:to-entity @(get (:refs entity-type) k))]
                           ;; Forcing if someone fed RMaps to select
                           (assoc m k (merge-conditions to-entity
                                                        op
                                                        (force (get conds1 k))
                                                        (force (get conds2 k))))))
                       (select-keys conds1 base-refs)
                       (clojure.set/intersection (set (keys conds2))
                                                 (set (keys (:refs entity-type)))))))))

(defn- normalize-conditions
  [entity-type conds]
  (reduce
   (fn [m k]
     (cond (contains? (:atts entity-type) k)
           (assoc m k (formed-condition (get conds k)))

           (and (contains? (:refs entity-type) k) (map? (force (get conds k))))
           (let [to-entity (:to-entity @(get (:refs entity-type) k))]
             ;;; Forcing if the conds is an RMap
             (assoc m k (normalize-conditions to-entity (force (get conds k)))))

           (contains? (:refs entity-type) k)
           (assoc m k (formed-condition (force (get conds k))))

           :else m))
   {}
   (keys conds)))

(defn- nil-ref-conditions
  [entity-type entity-alias params]
  (let [null-references (filter (fn [k] (and (contains? (:refs entity-type) k)
                                             (vector? (force (get params k)))))
                                (keys params))
        actual-condition (fn [fk v]
                           (if (= (force v) [:not [:= nil]])
                             (not* (=* (qualify entity-alias fk)
                                       nil))
                             
                             (=* (qualify entity-alias fk) nil)))]
    (mapcat (fn [k]
              (map (fn [[fk pk]] (actual-condition fk (get params k)))
                   (:fks-pks @(get-in entity-type [:refs k]))))
            null-references)))

(defn- nil-condition?
  [[k v]]
  (vector? v))

(defn- base-query
  "The basic queries from which others are composed, comprising
  attributes and null/not-null references only, on the given table"
  [entity-type params]
  (let [identity-aliases (util/map-hash-map repeat
                                            (col-names entity-type))
        entity-alias (generate-alias)
        table-query (cljql/table {(:table-name entity-type)
                                  entity-alias})
        base-conditions (->> params
                             (util/attribute-values entity-type)
                             (attribute-conditions entity-type entity-alias))
        nil-conditions  (nil-ref-conditions entity-type entity-alias params)]
    [(with-meta identity-aliases {:entity-alias entity-alias})
     (if-let [conditions (not-empty  (concat base-conditions nil-conditions))]
       (cljql/select
        table-query
        (where (apply-if-many and* conditions)))
       table-query)]))

(defn attribute-values
  "Selects the values of attributes from a hashmap, per entity-type"
  [entity-type m]
  (select-keys m
               (map (comp :col-name val)
                    (:atts entity-type))))

(defn- actual-references
  "Selects those references among query params that aren't set to
  null/not null conditions"
  [entity-type params]
  (->>  (util/reference-values entity-type
                               params)
       (filter (comp not nil-condition? force))
       (into {})))

(defn- recursive-join
  "Recursively builds join statements for references contained in the
  params hash"
  [query source-alias reference params]
  (let [entity-type (:to-entity @reference)
        att-params  (attribute-values entity-type params)
        [aliases base-join] (join-statement query
                                            source-alias
                                            reference
                                            att-params)
        entity-alias (:entity-alias (meta aliases))
        ;; We are just filtering a previous query, no need to actually
        ;; bring the data        
        empty-join (project base-join [])]
    (reduce (fn [q [r reference-params]]
              (recursive-join q
                              entity-alias
                              (get (:refs entity-type) r)
                              (force reference-params)))
            empty-join
            (actual-references entity-type params))))

(defn full-select
  [entity-type params]
  (let [[aliases query] (base-query entity-type params)

        entity-alias    (:entity-alias (meta aliases))
        reference-filtered-query (reduce (fn [q [reference r-params]]
                                           (recursive-join q
                                                           entity-alias
                                                           (get (:refs entity-type)
                                                                reference)
                                                           (force r-params)))
                                         query
                                         (actual-references entity-type params))
        query-with-columns (project-aliases [aliases reference-filtered-query]
                                            (col-names entity-type))]
    [aliases query-with-columns]))

(defn select
  ([entity-type]
     (select entity-type {}))
  ([entity-type params]
     (->TQuery entity-type (normalize-conditions entity-type params) false nil)))

(defn filter-query
  "Narrows the conditions of a query by applying AND to individual
  fields of conditions, recursively"
  ([query params]
     (assoc query :criteria (merge-conditions (:entity-type query) :and (:criteria query) params))))

(defn broaden-query
  "Broadens the conditions of a query by applying OR to individual
  fields of the record"
  ([query params]
     (assoc query :criteria (merge-conditions (:entity-type query) :or (:criteria query) params))))

(defn reference-hash-map
  "Returns a hashmap of the columns associated to this record's
  references"
  [reference reference-map]
  (let [reference-pk-values (second (pk-hash-map reference-map))]
    (util/map-hash-map (fn [[fk pk]]
                         [fk (get reference-pk-values pk)])
                       (:fks-pks reference))))

(defn not-null-valid?
  "Validates a record on whether any of its fields violates null
  constraints"
  [record-map & [insert?]]
  (let [entity-type (.entity-type record-map)
        not-nullables (fn [ks [k a]]
                        (if (not-nullable? (force a)) 
                          (cons k ks)
                          ks))
        not-nullable-keys (concat (reduce not-nullables
                                          '()
                                          (:atts entity-type))
                                  (reduce not-nullables
                                          '()
                                          (:refs entity-type)))
        not-nullable-in-context? (fn [k]
                                   (let [v (get record-map k)]
                                     (if insert?
                                       (and (nil? v)
                                            (not (:auto-inc (get-in entity-type
                                                                    [:atts k]))))
                                       (and (or (contains? record-map k)
                                                (:pk (get-in entity-type [:atts k])))
                                            (nil? (force v))))))]
    ;; If inserting, it's invalid if not present or nil
    ;; If updating, it's invalid only if present and specifically nil
    (not-empty (filter not-nullable-in-context?
                       not-nullable-keys))))

(defn invalid?
  "Validates record-map according to defined contraints. Currently, it only supports null-checks."
  ([record-map]
     (invalid? record-map :insert))
  ([record-map operation]
     (let [null-fields (not-null-valid? record-map (= operation :insert))]
       (not-empty (zipmap null-fields (repeat :null))))))

(defn exists?
  "Checks whether the given record exists, querying by PK"
  [record-map]
  (let [[_ pk] (pk-hash-map record-map)
        [_ query] (select-by-pk record-map)
        at-most-one (cljql/limit query 1)]
    (not (or (some nil? (vals pk)) (empty? @at-most-one)))))

(defn insert!
  "Inserts a single record-map into the database, non-recursively. Returns the key generated by the database, if any."
  [record-map]
  ;; ClojureQL's conj! procedure doesn't return the generated key,
  ;; which doesn't let you then get the generated object, so we wrote
  ;; our own
  (let [entity-type (.entity-type record-map)
        auto-inc    (-> :auto-inc
                        (filter (vals (:atts entity-type)))
                        (first)
                        :col-name)
        table-name  (:table-name entity-type)

        ref-values  (->> (:refs entity-type)
                         (map (fn [[key-name reference]]
                                (reference-hash-map @reference
                                                    (force (get record-map key-name)))))
                         
                         (reduce merge))
        att-values  (reduce (fn [values [k att]]
                              (if-not (and (not (contains? record-map (get record-map k)))
                                           (:auto-inc att))
                                (assoc values (:col-name att) (get record-map k))
                                values))
                            {}
                            (:atts entity-type))]
    (if-let [fields (not-null-valid? record-map true)]
      (throw (IllegalArgumentException. (str "Invalid null fields: "
                                             (clojure.string/join " " fields))))
      (let [db-values     (util/map-hash-map
                           (fn [[k val]]
                             [k (coerce-type (get @(:col-types entity-type) k)
                                             val)])
                           (merge att-values ref-values))

            result (sql/insert-record table-name
                                             db-values)]
        (if (map? result)
          (get result auto-inc)
          result)))))

(defn update!
  "Updates a record non-recursively. Returns the number of rows affected."
  [record-map]
  ;; We are writing a custom update procedure because ClojureQL's
  ;; update! didn't return the number of rows affected, which may be
  ;; occasionally useful
  (let [entity-type (.entity-type record-map)
        table-name  (:table-name entity-type)
        [_ pk-values] (pk-hash-map record-map)
        att-values  (attribute-values (.entity-type record-map)
                                      record-map)
        reference->maps (util/map-hash-map
                         (fn [[k, r]]
                           [@r (force (get record-map k))])
                         (:refs entity-type))
        ref-values      (reduce-kv (fn [m r vs]
                                     (merge m (reference-hash-map r vs)))
                                   {}
                                   reference->maps)
        column-values  (merge att-values ref-values)
        ;; We're going to depend on the ordering of iteration of a
        ;; hash map, but Clojure does not give any guarantees.
        ;; Paranoia works always
        column-keys (vec (keys column-values))
        predicate-keys (vec (keys  pk-values))
        predicate-string (->> (map (fn [c] (format "%s=?" (name c)))
                                   predicate-keys)
                              (clojure.string/join " AND "))
        column-string (->> (map (fn [c] (format "%s=?" (name c)))
                                column-keys)
                           (clojure.string/join ", "))
        update-string (format "UPDATE %s SET %s WHERE %s"
                              (name (:table-name entity-type))
                              column-string
                              predicate-string)
        atts-by-col-names (group-by :col-name (vals (:atts entity-type)))
        get-and-coerce (fn [m k]
                         (coerce-type (get-in atts-by-col-names [k 0 :data-type])
                                      (get m k)))
        params (concat (map (partial get-and-coerce column-values) column-keys)
                       (map (partial get-and-coerce pk-values) predicate-keys))]
    (if-let [fields (not-null-valid? record-map)]
      (throw (IllegalArgumentException. (str "Invalid null fields: "
                                             (clojure.string/join " " fields))))
      (first (sql/do-prepared update-string params)))))

(defn delete!
  "Deletes a given record from the database. Returns the number of rows affected"
  [record-map]
  (let [table-name (:table-name (.entity-type record-map))
        atts       (:atts (.entity-type record-map))
        [_ pk-values] (pk-hash-map record-map)
        pk-keys   (keys pk-values)
        predicate-string  (->> (map (fn [k]
                                      (format "%s=?" (name k)))
                                    pk-keys)
                               (clojure.string/join " AND "))]
    
    (first (sql/delete-rows table-name (cons predicate-string
                                             (for [k pk-keys]
                                               (coerce-type (get atts k)
                                                            (get pk-values k))))))))

(defn save!
  "General-purpose method for saving or inserting a record with all its fields. It's slower due to having to make more queries and checks, so buyer beware."
  [record-map])

(defn refresh
  "Re-queries the record-map to obtain possible changes from the database"
  [record-map]
  (let [entity-type (.entity-type record-map)
        [_ pk] (pk-hash-map record-map)
        [aliases query] (select-by-pk-row entity-type pk)]
    (row->record-map entity-type aliases (first @query))))

(defn r=
  "Compares two record-maps by identity, i.e. PK"
  [this that]
  (= (pk-hash-map this) (pk-hash-map that)))

(defn rassoc-in
  "Assoc-in for RMaps. It forces the nested maps as needed"
  [m [k & ks] v]
  (if ks
    (assoc m k (rassoc-in @(get m k) ks v))
    (assoc m k v)))

(defn count-records
  [query]
  (assoc query :count? true))

(defn limit-records
  [n query]
  (assoc query :limit n))
