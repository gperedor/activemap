(ns activemap.core-test
  (:use clojure.test
        activemap.core
        activemap.meta.types)
  (:require [activemap.fixtures :as fixtures]
            [activemap.test-util :as util]
            [activemap.util :as handy]
            [clojureql.core :as cljql]
            [clojure.java.jdbc :as sql]
            [clojure.set :as sets]))

(util/declare-dynamic state city travel driver artificial)

(defmacro result-set
  [expr]
  `(set (map util/recursive-force (deref ~expr))))

;;; Just map-contains? with the arguments swapped.
;;; Hey, the names must match the functionality!
(defn- partial-match?
  [subset superset]
  (util/map-contains? superset subset))

(defn with-fixtures [body]
  
  (binding [city   fixtures/city
            driver fixtures/driver
            state  fixtures/state
            travel fixtures/travel
            artificial fixtures/artificial]
    (util/with-setup fixtures/tables
      (body))))

(use-fixtures :each
  with-fixtures)

(deftest single-join-test
  (let [[aliases result-query] (join-statement (cljql/table {(:table-name city) :city_alias})
                                               :city_alias
                                               (:state (:refs city)))
        state-name-alias (:name (sets/map-invert aliases))]
    (is (= (set  [{:name            "Seattle"
                   :foundation      nil
                   :state_name      "Washington"
                   state-name-alias "Washington"}
                  {:name            "Vancouver"
                   :foundation      (util/make-date 1857 1 23)
                   :state_name      "Washington"
                   state-name-alias "Washington"}
                  {:name            "Los Angeles"
                   :foundation      (util/make-date 1781 9 4)
                   :state_name      "California"
                   state-name-alias "California"}
                  {:name            "San Francisco"
                   :foundation      (util/make-date 1776 6 29)
                   :state_name      "California"
                   state-name-alias "California"}])
           (result-set result-query)))))

(deftest double-join-test
  (let [[city-aliases destination-query] (join-statement
                                          (cljql/table {(:table-name travel) :travel_alias})
                                          :travel_alias
                                          (:destination (:refs travel)))
        [state-aliases state-query] (join-statement destination-query
                                                    (:entity-alias (meta city-aliases))
                                                    (:state (:refs city)))
        
        city-name-alias  (:name  (sets/map-invert city-aliases))
        state-name-alias (:name  (sets/map-invert state-aliases))
        sample-travel    {:destination_city_name       "Seattle"
                          :destination_city_state_name "Washington"
                          city-name-alias              "Seattle"
                          state-name-alias             "Washington"}]

    (is (some (partial partial-match? sample-travel)
              (result-set state-query)))))

(defn split-map
  "Splits a hash map into a vector of hash maps, according to a collection of key sets. A last map will contain entries not found in any key set"
  [m key-sets]
  (let [l (count key-sets)]
    (reduce (fn [maps [k v]]
              (loop [i 0]
                (cond (= i l) (assoc-in maps [l k] v)
                      (contains? (nth key-sets i) k) (assoc-in maps [i k] v)
                      :else (recur (inc i)))))
            (into [] (repeat (inc l) {}))
            m)))

(def city-aliases
  (with-meta {:g__1234  :name
              :g__2345 :foundation
              :g__3456 :state_name}
    {:entity-alias :g__4321}))

(def travel-aliases
  (with-meta {:g__4567  :travel_date
              :g__1234 :origin_city_name
              :g__2345 :origin_city_state_name
              :g__9876 :destination_city_name
              :g__3456 :destination_city_state_name}
    {:entity-alias :g__6543}))

(def city-row
  (handy/equivalent-subset (first fixtures/jdbc-cities)
                           (sets/map-invert city-aliases)))

(def travel-row
  (handy/equivalent-subset (first fixtures/jdbc-travels)
                           (sets/map-invert travel-aliases)))

(deftest row->record-map-test
  (is (= (util/recursive-force (row->record-map city
                                                city-aliases
                                                city-row))
         fixtures/los-angeles))
  
  (is (util/map-contains?
       (util/recursive-force (row->record-map travel
                                              travel-aliases
                                              travel-row))
       {:date (util/make-date 2013 1 1)
        :origin fixtures/los-angeles
        :destination fixtures/seattle}))

  (is (= (row->record-map city city-aliases nil))
      nil))

(deftest hash-map->record-map-test
  (let [destination {:name       "City1"
                     :foundation (util/make-date 1500 1 1)
                     :state      {:name "State1"}}
        travel-map  {:date        (util/make-date 2013 12 11)
                     :destination destination}]
    (is (= (util/recursive-force (hash-map->record-map travel travel-map))
           (assoc travel-map :origin nil)))

    (is (instance? activemap.meta.types.RMap
                   (hash-map->record-map travel travel-map)))))

(def origin-hash       {:origin_city_name       "Origin 1"
                        :origin_city_state_name "Origin State 1"})
(def destination-hash  {:destination_city_name       "Destination 1"
                        :destination_city_state_name "Destination State 1"})
(def travel-hash        (merge {:id nil
                                :travel_date (util/make-date 2013 10 10 )}
                               origin-hash
                               destination-hash))
(deftest pk-hash-map-test
  (let [travel-record-map (hash-map->record-map travel
                                                {:id nil
                                                 :date
                                                 (:travel_date travel-hash)
                                                 :origin {:name "Origin 1"
                                                          :state {:name "Origin State 1"}}
                                                 :destination {:name "Destination 1"
                                                               :state {:name "Destination State 1"}}})]

    (is (= [{} {:id nil}]
           (pk-hash-map travel-record-map)))
    (is (= [{} {}]
           (pk-hash-map {})))
    (is (= [{} {}]
           (pk-hash-map nil)))
    (is (= [{} {:name "Dummy" :state_name nil}]
           (pk-hash-map (hash-map->record-map fixtures/city
                                              {:name "Dummy"}))))))

(deftest select-by-pk-test
  (let [driver-recordmap (hash-map->record-map
                          fixtures/driver
                          {:name      "Juana Paredes"
                           :rut       "11111111-1"
                           :residence fixtures/los-angeles})
        [aliases query] (select-by-pk driver-recordmap)]
    (is  
     (= #{{:name "Juana Paredes"
           :rut "11111111-1"
           :birthday (util/make-date 1974 10 4)
           :residence_city_name "Los Angeles"
           :residence_city_state_name "California"
           :favorite_book_id 1}}
        (result-set query)))))

(def driver-row {:name "Juana Paredes" :rut "11111111-1"
                 :birthday  (util/make-date 1974 10 4)
                 :residence_city_name "California"
                 :residence_city_state_name "Los Angeles"})

(def driver-hash {:name "Juana Paredes" :rut "11111111-1"
                  :birthday  (util/make-date 1974 10 4)
                  :residence fixtures/los-angeles})

(deftest lazy-ref-test
  (is (= (:residence driver-hash)
         (util/recursive-force
          @(lazy-ref driver
                     (dissoc driver-row
                             :residence_city_name
                             :residence_city_state_name)
                     {}
                     (get-in driver [:refs :residence]))))))

(deftest select-query-test
  (is
   ;;;; With no private fields, Clojure types are forever
   ;;;; so we test the constructors
   (= (select travel {:destination {:name "Los Angeles"}
                      :date [:> (util/make-date 1999 12 31)]
                      :origin      {:state {:name "Washington"}}})
      (->TQuery travel
                {:destination {:name [:= "Los Angeles"]}
                 :origin {:state {:name [:= "Washington"]}}
                 :date [:> (java.sql.Date. (.getTime (util/make-date 1999 12 31)))]}
                false
                nil))
   "Consistent interface of query types")

  (is
   (= (result-set (select city))
      #{fixtures/san-francisco
        fixtures/los-angeles
        fixtures/seattle
        fixtures/vancouver})
   "A table-wide select should return all records")

  (is (= #{fixtures/seattle}
         (result-set (select city {:name "Seattle"})))
      "Querying for the attributes of a single row should return the corresponding record")

  (is (= (result-set (select travel {:destination
                                     {:state
                                      {:name "Washington"}}}))
         (set [{:id 0
                :origin      fixtures/los-angeles
                :destination fixtures/seattle
                :date        (util/make-date 2013 1 1)}
               {:id 2
                :origin      fixtures/san-francisco
                :destination fixtures/vancouver
                :date        (util/make-date 2013 1 12)}
               ]))
      "Querying for attributes of references should return the containing records")
  
  (is (= (result-set (select travel {:date [:> (util/make-date 2013 1 15)]}))
         #{{:id 1
            :origin fixtures/san-francisco
            :destination fixtures/los-angeles
            :date (util/make-date 2013 2 1)}})
      "Predicates different from equality must be supported")
  
  (is (= (result-set
          (select city
                  {:foundation [:or [:< (util/make-date 1777 1 1)] nil]})))
      
      "Composition of predicates by logical operators must be supported")
  
  (is (= (result-set (select driver {:residence {:name "Los Angeles"}
                                     :favorite-book {:publication-year 1605}}))
         #{{:name "Juana Paredes"
            :rut       "11111111-1"
            :birthday  (util/make-date 1974 10 4)
            :residence fixtures/los-angeles
            :favorite-book  {:id 1
                             :title "El ingenioso hidalgo don Quijote de la Mancha"
                             :publication-year  1605
                             :author {:name "Miguel de Cervantes"
                                      :id 1
                                      :nationality "Spaniard"}}}}))

  (is (= (result-set (select driver {:favorite-book nil}))
         #{{:rut                       "2222222-8"
            :name                      "Alfredo Chico"
            :birthday                  (util/make-date 1970 1 1)
            :residence                 fixtures/seattle
            :favorite-book nil}})
      "Search by null-references is supported")
  
  (is (= (result-set (select driver {:favorite-book [:not nil]}))
         
         #{{:rut                       "11111111-1"
            :name                      "Juana Paredes"
            :birthday                  (util/make-date 1974 10 4)
            :residence                 fixtures/los-angeles
            :favorite-book             {:title "El ingenioso hidalgo don Quijote de la Mancha"
                                        :publication-year  1605
                                        :author {:name "Miguel de Cervantes"
                                                 :nationality "Spaniard"
                                                 :id 1}
                                        
                                        :id 1}}}))

  
  (is (= (result-set (select fixtures/city
                             (hash-map->record-map fixtures/city
                                                   fixtures/san-francisco)))
         #{fixtures/san-francisco})
      "Select works with RMap-parameters")

  (is (= (result-set (filter-query
                      (select fixtures/travel
                              {:destination {:state {:name "Washington"}}})
                      {:destination (hash-map->record-map fixtures/city
                                                          fixtures/vancouver)}))
         #{{:id 2
            :origin fixtures/san-francisco
            :destination fixtures/vancouver
            :date (util/make-date 2013 1 12)}})
      "Merging conditions with RMaps")

  ;;; TBD
  ;; (is (=  (result-set (select city
  ;;                             {:state [:or {:name "California"}
  ;;                                      {:name "Washignton"}]})))
  ;;     "NOT YET IMPLEMENTED: Logical operators at the reference level should result in a valid query")
  )

;; (deftest coll-test
  
;;   (let [referrers (->> @(select fixtures/unnatural {:id 2})
;;                        (first)
;;                        (:referrers)
;;                        force
;;                        handy/dbg
;;                        (map attributes)
;;                        (set))
;;         ]
;;     (is (= referrers
;;            (set '({:id 4} {:id 5} {:id 6}))))))

(deftest filter-query-test
  (is (= (result-set (filter-query (select city) {:name "Los Angeles"}))
         #{fixtures/los-angeles})
      "Filtering an empty query")
  
  (let [base-query (select travel {:date [:> (util/make-date 2012 1 1)]})]
    (is (= (result-set (filter-query base-query
                                     {:destination {:name "Los Angeles"}
                                      :date [:< (util/make-date 3000 1 1)]}))
           #{{:id 1
              :origin fixtures/san-francisco
              :destination fixtures/los-angeles         
              :date (util/make-date 2013 2 1)}})
        "Filtering a more complex query")))

(deftest broaden-query-test
  (is (= (result-set (broaden-query (select city {:name "Los Angeles"})
                                    {:name "Seattle"}))
         #{fixtures/los-angeles
           fixtures/seattle})
      "Broadening a query for an entity's attributes")
  (is (= (result-set (broaden-query (select city {:state {:name "California"}})
                                    {:state {:name "Washington"}}))
         #{fixtures/los-angeles
           fixtures/seattle
           fixtures/san-francisco
           fixtures/vancouver})
      "Broadening a query for an entity's references"))

(deftest count-records-test
  (is (= @(count-records (select city))
         4)
      "count-records returns the number of elements in a table")
  (is (= @(count-records (select travel {:state {:name "Washington"}}))
         3)
      "count-records returns the number of records from a complex query"))

(deftest single-insert-test
  (let [state-record-map (hash-map->record-map state
                                               {:name "Oregon"})
        result (insert! state-record-map)]
    (is (= (first @(select state {:name "Oregon"}))
           state-record-map)
        "New row can be queried from database")
    (is (thrown? IllegalArgumentException
                 (insert! (hash-map->record-map city
                                                {:state {:name "California"}})))))
  (let [travel-record-map (hash-map->record-map fixtures/travel
                                                {:origin fixtures/vancouver
                                                 :destination fixtures/vancouver
                                                 :date (util/make-date 2012 1 1)})]
    (is (number? (insert! travel-record-map)))))

(deftest update-test
  (let [city-record-map (hash-map->record-map city
                                              fixtures/los-angeles)
        modified-city (assoc city-record-map
                        :foundation (util/make-date 2014 1 1))
        bogus-city    (hash-map->record-map city
                                            (assoc fixtures/los-angeles
                                              :name "Bogus City"
                                              :foundation (util/make-date 2014 2 2)))]
    (is (= (update! modified-city)
           1)
        "Returns one (1) on successful update")
    (is (=  (util/recursive-force modified-city)
            (first (result-set (select city
                                       {:name "Los Angeles"
                                        :state {:name "California"}}))))
        "Updated row actually reflects changes")
    (is (= (update! bogus-city)
           0)
        "Non-existent record should not be reported as updated")))

(deftest delete-test
  (is (= (delete! (hash-map->record-map city fixtures/san-francisco))
         1)
      "Specific row should be affected")
  (is (empty? @(select city {:name "San Francisco"}))
      "Row should not be found in database"))

(deftest refresh-test
  (let [city-record-map (hash-map->record-map city
                                              fixtures/los-angeles)]
    (cljql/update! (cljql/table :city) (cljql/where (= :name "Los Angeles")) {:foundation (util/sql-date 2014 1 1)})
    (is (= (util/recursive-force (refresh city-record-map))
           {:name "Los Angeles"
            :state {:name "California"}
            :foundation (util/make-date 2014 1 1)}))))

(deftest transaction-test
  (let [test-city (hash-map->record-map city
                                        {:name "Test 1"
                                         :state {:name "Washington"}
                                         :foundation (util/make-date 2014 1 1)})]
    (sql/transaction
     (insert! test-city)
     (insert! (assoc test-city :name "Test 2"))
     (sql/set-rollback-only))
    (is (empty? @(select city {:name [:or [:= "Test 1"]
                                      [:= "Test 2"]]})))))

(deftest r=-test
  (is (r= (hash-map->record-map city fixtures/los-angeles)
          (hash-map->record-map city fixtures/los-angeles)))
  (is (not (r= (hash-map->record-map city fixtures/los-angeles)
               (hash-map->record-map city fixtures/san-francisco)))))

(deftest exists?-test
  (is (false? (exists?
               (hash-map->record-map fixtures/city {:name "Dummy"})))
      (true?  (exists?
               (hash-map->record-map fixtures/city fixtures/san-francisco)))))

(deftest coll-test
  (is (util/coll-equals @(:referrers (first @(select fixtures/unnatural
                                                     {:id 2})))
                        (map (fn [m] (row->record-map fixtures/artificial
                                                      {}
                                                      m))
                             (list {:id 4 :arbitrary_id 2}
                                   {:id 5 :arbitrary_id 2}
                                   {:id 6 :arbitrary_id 2}))))
  (is (util/coll-equals @(:referrers (first @(select fixtures/unnatural
                                                     {:id 3})))
                        ())))

(deftest validation-test
  (is (= {:state :null}
         (invalid? (hash-map->record-map fixtures/city
                                         {:name "Test"}))))
  (is (not (invalid? (hash-map->record-map fixtures/state
                                           {:name "Test"}))))
  (is (not (invalid? (hash-map->record-map fixtures/artificial
                                           {:unnatural-1 0})
                     :insert))))

;(run-tests)
