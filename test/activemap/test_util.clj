(ns activemap.test-util
  (:use activemap.core)
  (:require [clojureql.core :as cljql]
            [clojure.java.jdbc :as sql]
            [lobos.core :as lobos]
            clojure.data
            clojure.string))

(def hsqldb
  {:classname   "org.hsqldb.jdbcDriver"
   :subprotocol "hsqldb:mem"
   :user        "sa"
   :password    ""
   :auto-commit true                                    
   :subname     "//localhost/test"})

(def mysql
  {:classname   "com.mysql.jdbc.Driver"
   :subprotocol "mysql"
   :user        "root"
   :password    ""
   :auto-commit true                                    
   :subname     "//localhost/test"})

(def postgres
  {:classname   "org.postgresql.Driver"
   :subprotocol "postgresql"
   :user        "postgres"
   :password    ""
   :auto-commit true
   :subname     "//localhost/test"})

(def postgres-benchmarks
  {:classname   "org.postgresql.Driver"
   :subprotocol "postgresql"
   :user        "postgres"
   :password    ""
   :auto-commit true
   :subname     "//localhost/benchmarks"})

(def ^:dynamic *db* postgres)

(defn setup
  [database tables]
  (sql/with-connection database
    (doseq [table-name (keys tables)]
      (apply sql/create-table (get-in tables [table-name :structure])))
    (doseq [stmt (get (:pre-processing (meta tables))
                      (:subprotocol database))]
      (sql/do-commands stmt))
    (doseq [table-name (keys tables)]
      @(-> (cljql/table database table-name)
           (cljql/conj! (get-in tables [table-name :data]))))))

(defn tear-down
  [database tables]
  (sql/with-connection database
   (doseq [table tables]
     (sql/drop-table (first table))))
  (doseq [stmt (get (:post-processing (meta tables))
                    (:subprotocol database))]
    (sql/do-commands stmt)))

;(tear-down db/db)

(defn with-setup*
  [db tables body]
  (sql/with-connection db
    (try
      (setup db tables)
      (body)
      (catch Exception e
        (.printStackTrace e))
      (finally
        (tear-down db tables)))))

(defmacro with-setup
  ([tables & body]
     `(with-setup* ~*db* ~tables (fn [] ~@body))))

(defn make-date
  [year month day]
  (.getTime (java.util.GregorianCalendar. year (dec month) day)))

(defn sql-date
  [year month day]
  (-> (make-date year month day)
      (.getTime)
      (java.sql.Date.)))

(defmacro declare-dynamic
  "Vanity macro; forward-declares a bunch of variables to be dynamic"
  [& variables]
  `(do
     ~@(map (fn [var-symbol]
              `(declare ~(with-meta var-symbol
                           (assoc (meta var-symbol) :dynamic true))))
            variables)))

(defn map-contains?
  "Tests whether the right-hand map is strict subset of the left-hand map"
  [superset subset]
  (let [[deficiency excess intersection]  (clojure.data/diff superset subset)]
    (empty? excess)))

(defn recursive-force
  [m]
  (let [contents (map (fn [[k v]]
                        (->> (if (delay? v)
                               (recursive-force @v)
                               v)
                             (vector k)))
                      m)]
    (if (empty? contents)
      nil
      (into {} contents))))

(defn coll-equals
  [coll1 coll2]
  (not-any? (fn [[pk elems]]
              (not= (count elems) 2))
            (group-by pk-hash-map (concat coll1 coll2))))
