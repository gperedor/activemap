(ns activemap.fixtures
  (:use activemap.meta.types
        activemap.test-util
        activemap.meta.ddl)) 

(declare city artificial)

;;; JDBC data
;;; TODO: make the PKs composite with ALTER TABLE
(def jdbc-state '(:state [:name "VARCHAR(255)" "PRIMARY KEY"]))
(def jdbc-city  '(:city  [:name "VARCHAR(255)" "PRIMARY KEY"]
                         [:state_name "VARCHAR(255)"]
                         [:foundation "DATE"]))
(def jdbc-travel '(:travel [:travel_date "DATE"]
                           [:origin_city_name "VARCHAR(255)"]
                           [:origin_city_state_name "VARCHAR(255)"]
                           [:destination_city_name "VARCHAR(255)"]
                           [:destination_city_state_name "VARCHAR(255)"]
                           [:id "INTEGER"]))

(def jdbc-driver '(:driver [:name "VARCHAR(255)"]
                           [:rut "VARCHAR(20)"]
                           [:birthday "DATE"]
                           [:residence_city_state_name "VARCHAR(255)"]
                           [:residence_city_name "VARCHAR(255)"]
                           [:favorite_book_id "INTEGER"]))

(def jdbc-author  '(:author [:name "VARCHAR(255)"]
                            [:nationality "VARCHAR(255)"]
                            [:id "INTEGER"]))

(def jdbc-book    '(:book [:id "INTEGER"]
                          [:title "VARCHAR(255)"]
                          [:author_id "INTEGER"]
                          [:publication_year "INTEGER"]))

(def jdbc-artificial '(:artificial [:id "INTEGER PRIMARY KEY"]
                                   [:arbitrary_id "INTEGER"]))

(def jdbc-unnatural '(:unnatural [:id "INTEGER"]
                                 [:unnatural_1 "VARCHAR(255)"]))

(def jdbc-synthetic '(:synthetics [:id "INTEGER"
                                   :synthetics_1 "INTEGER"]))

(def jdbc-synthetic-unnatural '(:synthetics_unnatural [:synthetics_id "INTEGER"
                                                       :unnatural_id "INTEGER"]))

(def jdbc-travels [{:origin_city_name            "Los Angeles"
                    :origin_city_state_name      "California"
                    :destination_city_name       "Seattle"
                    :destination_city_state_name "Washington"
                    :travel_date                 (sql-date 2013 1 1)}
                   {:origin_city_name            "San Francisco"
                    :origin_city_state_name      "California"
                    :destination_city_name       "Los Angeles"
                    :destination_city_state_name "California"
                    :travel_date                 (sql-date 2013 2 1)}
                   {:origin_city_name            "San Francisco"
                    :origin_city_state_name      "California"
                    :destination_city_name       "Vancouver"
                    :destination_city_state_name "Washington"
                    :travel_date                 (sql-date 2013 1 12)}])
(def jdbc-states [{:name "California"} {:name "Washington"}])
(def jdbc-cities [{:name       "Los Angeles"
                   :state_name "California"
                   :foundation (sql-date 1781 9 4)}
                  {:name       "San Francisco"
                   :state_name "California"
                   :foundation (sql-date 1776 6 29)}
                  {:name "Seattle"
                   :state_name "Washington"}
                  {:name "Vancouver"
                   :state_name "Washington"
                   :foundation (sql-date 1857 1 23)}])

(def jdbc-drivers [{:rut                       "11111111-1"
                    :name                      "Juana Paredes"
                    :birthday                  (sql-date 1974 10 4)
                    :residence_city_name       "Los Angeles"
                    :residence_city_state_name "California"
                    :favorite_book_id 1}
                   {:rut                       "2222222-8"
                    :name                      "Alfredo Chico"
                    :birthday                  (sql-date 1970 1 1)
                    :residence_city_name       "Seattle"
                    :residence_city_state_name "Washington"
                    :favorite_book_id nil}])

(def jdbc-books   [{:title "El ingenioso hidalgo don Quijote de la Mancha"
                    :publication_year  1605
                    :author_id 1
                    :id 1}
                   {:title "La Voragine"
                    :publication_year 1924
                    :author_id 2
                    :id 2}])

(def jdbc-authors [{:name "Miguel de Cervantes"
                    :nationality "Spaniard"
                    :id 1}
                   {:name "José Eustasio Rivera"
                    :nationality "Colombian"
                    :id 2}])

(def jdbc-artificials [{:id 1
                        :arbitrary_id nil}
                       {:id 2
                        :arbitrary_id nil}
                       {:id 3
                        :arbitrary_id 1}
                       {:id 4
                        :arbitrary_id 2}
                       {:id 5
                        :arbitrary_id 2}
                       {:id 6
                        :arbitrary_id 2}])

(def jdbc-unnaturals [{:id 1
                       :unnatural_1 "UA1"}
                      {:id 2
                       :unnatural_1 "U2"}
                      {:id 3
                       :unnatural_1 "U3"}])

(def pre-processing
  {"hsqldb:mem" ["ALTER TABLE ARTIFICIAL DROP COLUMN ID"
                 "ALTER TABLE ARTIFICIAL ADD COLUMN ID INTEGER GENERATED BY DEFAULT AS IDENTITY"
                 "ALTER TABLE TRAVEL DROP COLUMN ID"
                 "ALTER TABLE TRAVEL ADD COLUMN ID INTEGER GENERATED BY DEFAULT AS IDENTITY"]
   "postgresql" ["create sequence a_seq start 0 minvalue 0"
                 "alter table artificial alter column id set default nextval('a_seq')"
                 "create sequence t_seq start 0 minvalue 0"
                 "alter table travel alter column id set default nextval('t_seq')"]})

(def post-processing
  {"postgresql" ["drop sequence a_seq"
                 "drop sequence t_seq"]})

(def tables
  (with-meta 
    {:state      {:structure jdbc-state
                 :data      jdbc-states}
    :city       {:structure jdbc-city
                 :data      jdbc-cities}
    :travel     {:structure jdbc-travel
                 :data      jdbc-travels}
    :driver     {:structure jdbc-driver
                 :data      jdbc-drivers}
    :artificial {:structure jdbc-artificial
                 :data      jdbc-artificials}
    :unnatural  {:structure jdbc-unnatural
                 :data      jdbc-unnaturals}
    :author     {:structure jdbc-author
                 :data      jdbc-authors}
    :book       {:structure jdbc-book
                 :data      jdbc-books}}
    {:pre-processing  pre-processing
     :post-processing post-processing}))

;;; Activemap metadata
;;; TODO: the JDBC table structures will be _inevitably_ superseded
;;;       by this
(declare travel city state driver)

(defentity travel
  {:table-name :travel}
  (attribute :id   {:pk true :not-null true :data-type :integer :auto-inc true})
  (attribute :date {:col-name :travel_date :data-type :date
                    :not-null true})
  (reference :origin city {:not-null true
                           :fks-pks {:origin_city_name :name
                                     :origin_city_state_name :state_name}})
  (reference :destination city {:fks-pks {:destination_city_name :name
                                          :destination_city_state_name :state_name}
                                :not-null true}))

(defentity author
  {:table-name :author}
  (attribute :id {:pk true :not-null true :data-type :number})
  (attribute :nationality)
  (attribute :name {:not-null true}))

(defentity book
  {:table-name :book}
  (attribute :id {:pk true :data-type :number})
  (attribute :title {:not-null true})
  (attribute :publication-year {:data-type :integer})
  (reference :author author))

(defentity driver
  {:table-name :driver}
  (attribute :name {:not-null true})
  (attribute :rut {:pk true :not-null true})
  (attribute :birthday {:data-type :date})
  (reference :residence city {:fks-pks {:residence_city_name :name
                                        :residence_city_state_name :state_name}})
  (reference :favorite-book book {:fks-pks {:favorite_book_id :id}}))

(defentity state
  {:table-name :state}
  (attribute :name {:pk true :not-null true}))

(defentity city
  {:table-name :city}
  (attribute :name {:pk true :not-null true})
  (attribute :foundation {:data-type :date})
  (reference :state state {:pk true :not-null true}))

(defentity unnatural
  {:table-name :unnatural}
  (attribute :id {:pk true :not-null true :auto-inc true})
  (attribute :unnatural-1 {:data-type :integer})
  (collection :referrers artificial :arbitrary))

(defentity artificial
  {:table-name :artificial}
  (attribute :id {:pk true :not-null true :auto-inc true})
  (reference :arbitrary unnatural))

(defentity synthetic
  {}
  (attribute :id {:pk true :data-type :integer :auto-inc true})
  (collection-through :constructed artificial))

(def los-angeles
  {:name "Los Angeles"
   :state      {:name "California"}
   :foundation (make-date 1781 9 4)})


(def san-francisco
  {:name       "San Francisco"
   :state      {:name "California"}
   :foundation (make-date 1776 6 29)})

(def seattle
  {:name "Seattle"
   :state      {:name "Washington"}
   :foundation nil})

(def vancouver
  {:name  "Vancouver"
   :state {:name "Washington"}
   :foundation (make-date 1857 1 23)})
