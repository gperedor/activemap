create table state (
       name varchar(255) primary key
);

create table city (
       name varchar(255) primary key,
       state_name varchar(255),
       foundation date
);

create table travel (
       id integer,
       travel_date date,
       origin_city_name varchar(255),
       origin_city_state_name varchar(255),
       destination_city_name varchar(255),
       destination_city_state_name varchar(255)
);

create sequence tr_seq;

alter table travel alter column set default nextval('tr_seq');

create table driver (
       birthday date,
       name varchar(255),
       residence_city_state_name varchar(255),
       residence_city_name varchar(255),
       favorite_book_id integer
);

create table author (
       name varchar(255),
       id integer,
       nationality varchar(255)
);

create table book (
       id integer,
       title varchar(255),
       publication_year integer,
       author_id integer
);

create table artificial (
       id integer,
       string_pk varchar(255) primary key
);
