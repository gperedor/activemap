(defproject activemap "0.1.0-SNAPSHOT"
  :description "Activemap: turning your database tables into nested records"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.5.0"]
                 [org.clojure/core.match "0.2.0-alpha12"]

                 ;;; SQL abstractions
                 [clojureql "1.0.4"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [lobos "1.0.0-beta1"]

                 ;;; DB drivers
                 [hsqldb/hsqldb "1.8.0.10"]
                 [mysql/mysql-connector-java "5.1.28"]
                 [org.postgresql/postgresql "9.3-1100-jdbc41"]

                 ;;; Types
                 [potemkin "0.3.4"]]
  :profiles {:dev {:source-paths ["src" "benchmarks"]
                   :main activemap.benchmarks/run-benchmark}})
